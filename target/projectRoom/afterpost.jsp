<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="java.util.List"%>
<%@page import="com.model.Room" %>
<html>
<head>
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/fonts/font-awesome.min.css">
    <link rel="stylesheet" href="assets/fonts/ionicons.min.css">
    <link rel="stylesheet" href="assets/fonts/material-icons.min.css">
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=ABeeZee">
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Cookie">
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
    <link rel="stylesheet" href="assets/css/JLX-Footer-with-Icons.css">
    <link rel="stylesheet" href="assets/css/Navigation-with-Button.css">
    <link rel="stylesheet" href="assets/css/Navigation-with-Search.css">
    <link rel="stylesheet" href="assets/css/Pretty-Footer.css">
    <link rel="stylesheet" href="assets/css/Responsive-footer.css">
    <link rel="stylesheet" href="assets/css/Responsive-footer1.css">
    <link rel="stylesheet" href="assets/css/styles.css">
    <link rel="stylesheet" href="css/glyphicon.css">
    <link rel="stylesheet" href="css/custom.css">
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="assets/js/bs-animation.js"></script>




    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
          rel="stylesheet">

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

    <title>Room</title>
</head>
<body>
<div class="container">
    <nav
            class="navbar navbar-light navbar-expand-md navigation-clean-search">

        <a class="navbar-brand" href="afterlogin.jsp"> <img
                class="rounded img-fluid" src="img/newlogo.jpg" width="100px"
                height="100px"></a>
        <button class="navbar-toggler" data-toggle="collapse"
                data-target="#navcol-1">
            <span class="sr-only">Toggle navigation</span> <span
                class="navbar-toggler-icon"></span>
        </button>


        <div class="collapse navbar-collapse" id="navcol-1">
            <ul class="nav navbar-nav mx-auto">
                <li class="nav-item" role="presentation"><a
                        class="nav-link active" href="afterlogin.jsp">Home</a></li>
                <li class="nav-item" role="presentation"><a class="nav-link"
                                                            href="about.jsp">About Us</a></li>
                <li class="nav-item" role="presentation"><a class="nav-link"
                                                            href="contact.jsp">Contact Us</a></li>
            </ul>
            <form class="form-inline mr-auto" target="_self">
                <div class="form-group">
                    <%--@declare id="search-field"--%><label for="search-field"></label>
                </div>
            </form>
        </div>
    </nav>
</div>

<div class="pull-right">
    <p>
        <a href="post.jsp" class="btn btn-primary"><span class="glyphicon glyphicon-plus"></span> Add</a>
    </p>
</div>

<table class="table table-striped" border="2">
    <thead>
    <th>S.N.</th>
    <th>roomType</th>
    <th>price</th>
    <th>description</th>
    <th>location</th>
    <th>facilities</th>
    </thead>
        <%
            List<Room> roomList=(List<Room>)request.getAttribute("roomList");
            int i=1;
            if (roomList!=null)
            {
            for (Room room:roomList) {
            %><tr>
            <td><%=i%></td>
            <td><%=room.getRoomType()%></td>
            <td><%=room.getPrice()%></td>
            <td><%=room.getDescription()%></td>
            <td><%=room.getLocation()%></td>
            <td><%String[] facilities=room.getFacilities();
                for (String s:facilities) {
                    out.print(s+",");
                }

            %></td>
    <td>

        <a href="/home/update?id=<c:out value="${c.id}"/>" class="btn btn-success"><span class="glyphicon glyphicon-pencil"></span> </a>
        <a href="/home/delete?id=<c:out value="${c.id}"/>" class="btn btn-danger" onclick="return confirm('Are you Sure?')"><span class="glyphicon glyphicon-trash"></span> </a>
    </td>
</tr>

            <%i++;}}
        %>

</table>

<div
        style="background-color: rgba(58, 47, 47, 0); background-image: url(&amp;quot;assets/img/footer.png&amp;quot;); background-size: cover; background-repeat: no-repeat; height: 366px;">
    <div class="container">
        <h1 class="text-center"
            style="font-family: ABeeZee, sans-serif; font-size: 46px; color: rgb(0, 0, 0);">Room
            Finder</h1>
        <hr>
        <ul class="list-inline" id="text_decoration"
            style="margin-top: 30px; font-family: ABeeZee, sans-serif;">
            <li class="list-inline-item"><a href="#" class="decoration"
                                            style="color: rgb(0, 0, 0); font-size: 20px; margin-right: 10px;">Home</a></li>
            <li class="list-inline-item"><a href="#"
                                            style="font-size: 20px; color: rgb(0, 0, 0); margin-right: 10px;">Services</a></li>
            <li class="list-inline-item"><a href="#"
                                            style="font-size: 20px; color: rgb(0, 0, 0); margin-right: 10px;">About</a></li>
            <li class="list-inline-item"><a href="#"
                                            style="color: rgb(0, 0, 0); font-size: 20px; margin-right: 10px;">Terms</a></li>
            <li class="list-inline-item"><a href="#"
                                            style="color: rgb(0, 0, 0); font-size: 20px;">Privacy Policy</a></li>
        </ul>
        <p class="text-center" style="margin-top: 30px; margin-bottom: 15px;">
            <a href="#" style="font-size: 35px; margin-right: 30px;"><i
                    class="fa fa-facebook-square"
                    style="color: rgb(0, 0, 0); margin-right: 0px;"></i></a><a href="#"
                                                                               style="font-size: 35px; margin-right: 30px;"><i
                class="fa fa-instagram"
                style="color: rgb(0, 0, 0); margin-right: 0px;"></i></a> <a href="#"
                                                                            style="font-size: 35px; margin-right: 30px;"><i
                class="icon ion-social-twitter"
                style="color: rgb(0, 0, 0); margin-right: 2px;"></i></a><a href="#"
                                                                           style="font-size: 35px; margin-right: 30px;"><i
                class="icon ion-social-snapchat"
                style="color: rgb(0, 0, 0); margin-right: 0px;"></i></a><a href="#"
                                                                           style="font-size: 35px;"><i class="fa fa-google-plus-square"
                                                                                                       style="color: rgb(0, 0, 0);"></i></a>
        </p>
        <p class="text-center"
           style="color: rgb(193, 184, 184); font-family: ABeeZee, sans-serif; margin-bottom: 23px;">RoomFinder@
            2018</p>


    </div>
</div>

</body>
</html>
