<%@ page import="com.model.Room" %>
<%@ page import="com.model.LoginBean" %><%--
  Created by IntelliJ IDEA.
  User: root
  Date: 9/2/18
  Time: 3:42 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="searchone.css">


    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/fonts/font-awesome.min.css">
    <link rel="stylesheet" href="assets/fonts/ionicons.min.css">
    <link rel="stylesheet" href="assets/fonts/material-icons.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=ABeeZee">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Cookie">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
    <link rel="stylesheet" href="assets/css/JLX-Footer-with-Icons.css">
    <link rel="stylesheet" href="assets/css/Navigation-with-Button.css">
    <link rel="stylesheet" href="assets/css/Navigation-with-Search.css">
    <link rel="stylesheet" href="assets/css/Pretty-Footer.css">
    <link rel="stylesheet" href="assets/css/Responsive-footer.css">
    <link rel="stylesheet" href="assets/css/Responsive-footer1.css">
    <link rel="stylesheet" href="assets/css/styles.css">
    <link rel="stylesheet" href="css/glyphicon.css">
    <link rel="stylesheet" href="css/custom.css">
</head>
<body>

<div class="container">
    <nav class="navbar navbar-light navbar-expand-md navigation-clean-search" style="width: 100%;">

        <a class="navbar-brand" href="index.jsp">
            <img class="rounded img-fluid" src="img/newlogo.jpg" width="100px" height="80px"></a>
        <button class="navbar-toggler" data-toggle="collapse" data-target="#navcol-1">
            <span class="sr-only">Toggle navigation</span>
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navcol-1" style="margin-left: 140px; margin-right: -1250px;">
            <ul class="nav navbar-nav mx-auto" >
                <li class="nav-item" role="presentation"><a class="nav-link active" href="index.jsp">Home</a></li>
                <li class="nav-item" role="presentation"><a class="nav-link" href="about.jsp">About Us</a></li>
                <li class="nav-item" role="presentation" ><a class="nav-link" href="contact.jsp">Contact Us</a></li>
            </ul>
            <form class="form-inline mr-auto" action="roominfo.do" target="_self">
                <div class="form-group">
                    <%--@declare id="search-field"--%><label for="search-field"></label>
                </div>
            </form>
        </div>
    </nav>
    <hr>


<table>
    <%
       Room room= (Room) request.getAttribute("roominfo");

        if(room!=null){
            %>
        <tr>
            <h3>Room Information </h3>
            <hr />
            RoomType :<%=room.getRoomType()%><br/>
            Price : <%=room.getPrice()%><br/>
            Location : <%=room.getLocation()%><br/>
            Description :<%=room.getDescription()%><br/>
            Facilities :
            <%
                String[] s=room.getFacilities();
                for (String st:s) {
                    out.print(st+",");
                }
            %>
        <%
        LoginBean user = (LoginBean) request.getAttribute("userinfo");
        if(user != null) {
    %>
            <!-- Show user information  -->
    <hr />
    <h3>User Information </h3>
    <hr />
    Name :<%=user.getFirstname()%> <%=user.getLastname()%><br />
    Address :<%=user.getAddress()%> <br/>
    Contact :<%=user.getPhone()%> <br />
    Email :<%=user.getEmail()%>
    <%    }else {  %>
            <!-- Show alert saying "User Information Not avialable" -->
     <%   } %>
</tr>

    <%}else {%>
    <tr><h1>Room Information Not found!!! :(</h1></tr>
    <%}%>

</table>


<div style="background-color:rgba(58,47,47,0); background-image:url(&quot; assets/img/footer.png &quot;); background-size:cover; background-repeat:no-repeat; height:366px;">
    <div class="container">
        <h1 class="text-center" style="font-family:ABeeZee, sans-serif; font-size:46px; color:rgb(0,0,0);">Room Finder</h1>
        <hr>
        <ul class="list-inline" id="text_decoration" style="margin-top:30px;font-family:ABeeZee, sans-serif;">
            <li class="list-inline-item"><a href="#" class="decoration" style="color:rgb(0,0,0);font-size:20px;margin-right:10px;">Home</a></li>
            <li class="list-inline-item"><a href="#" style="font-size:20px;color:rgb(0,0,0);margin-right:10px;">Services</a></li>
            <li class="list-inline-item"><a href="#" style="font-size:20px;color:rgb(0,0,0);margin-right:10px;">About</a></li>
            <li class="list-inline-item"><a href="#" style="color:rgb(0,0,0);font-size:20px;margin-right:10px;">Terms</a></li>
            <li class="list-inline-item"><a href="#" style="color:rgb(0,0,0);font-size:20px;">Privacy Policy</a></li>
        </ul>
        <p class="text-center" style="margin-top:30px;margin-bottom:15px;"><a href="#" style="font-size:35px;margin-right:30px;"><i class="fa fa-facebook-square" style="color:rgb(0,0,0);margin-right:0px;"></i></a><a href="#" style="font-size:35px;margin-right:30px;"><i class="fa fa-instagram" style="color:rgb(0,0,0);margin-right:0px;"></i></a>
            <a
                    href="#" style="font-size:35px;margin-right:30px;"><i class="icon ion-social-twitter" style="color:rgb(0,0,0);margin-right:2px;"></i></a><a href="#" style="font-size:35px;margin-right:30px;"><i class="icon ion-social-snapchat" style="color:rgb(0,0,0);margin-right:0px;"></i></a><a href="#"
                                                                                                                                                                                                                                                                                                             style="font-size:35px;"><i class="fa fa-google-plus-square" style="color:rgb(0,0,0);"></i></a></p>
        <p class="text-center" style="color:rgb(193,184,184);font-family:ABeeZee, sans-serif;margin-bottom:23px;">RoomFinder@ 2018</p>


    </div>
</div>

</div>
<script src="assets/js/jquery.min.js"></script>
<script src="assets/bootstrap/js/bootstrap.min.js"></script>
<script src="assets/js/bs-animation.js"></script>

</body>
</html>
