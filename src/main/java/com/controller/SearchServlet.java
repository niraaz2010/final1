package com.controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.connection.Mysql;
import com.cosineSimilarity.Data;
import com.cosineSimilarity.DocumentParser;
import com.model.Room;

@WebServlet("/search.do")
public class SearchServlet extends HttpServlet {
	
	public SearchServlet(){
		
	}
	
	private static final long serialVersionUID = 1L;
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
    	String roomType=request.getParameter("roomType");
//    	Double priceFrom=Double.parseDouble(request.getParameter("priceFrom"));
//    	Double priceTo=Double.parseDouble(request.getParameter("priceTo"));
    	String facilities = "";
    	String water = request.getParameter("water_facilities");
    	if(water != null) {
    		facilities = addFacilities(facilities, water);
    	}
    	String electricity = request.getParameter("electricity_facilities"); 
    	if(electricity != null) {
    		facilities = addFacilities(facilities, electricity);
    	}
    	 
    	String internet = request.getParameter("internet_facilities"); 
    	if(internet != null) {
    		facilities = addFacilities(facilities, internet);
    	}
    	 
    	String parking = request.getParameter("parking_facilities"); 
    	if(parking != null) {
    		facilities = addFacilities(facilities, parking);
    	}
    	int p1=Integer.parseInt(request.getParameter("priceFrom"));
    	int p2=Integer.parseInt(request.getParameter("priceTo"));
    	
    	
    	Data query1=new Data();
    	query1.setFacilities(facilities.split(","));
    	query1.setRoomType(roomType);
    	Connection con=null;

	   	try
    	{
    		con=Mysql.createConnection();
    		String query = "SELECT * from room where price between ? and ?";
    		PreparedStatement statement=con.prepareStatement(query);
    		statement.setInt(1,p1);
    		statement.setInt(2,p2);

    		ResultSet resultSet;
    		resultSet=statement.executeQuery();

    		List<Room> datas = new ArrayList<Room>();

    		System.out.println("details");
    		while (resultSet.next()){
				Room data=new Room();
    			int id=resultSet.getInt("room_id");
    			String rType=resultSet.getString("roomType");
    			Double price=resultSet.getDouble("price");
    			String description=resultSet.getString("description");
    			String location=resultSet.getString("location");
    			String rfacilities=resultSet.getString("facilities");
    			System.out.println(id+" "+rType+" "+price+" "+description+" "+location+" "+rfacilities);
    			
    			data.setId(id);
    			data.setPrice(price);
    			data.setDescription(description);
    			data.setRoomType(rType);
    			data.setLocation(location);
    			data.setFacilities(rfacilities);

    			//yeta halne

    			datas.add(data);

    		}
    		System.out.println("list :"+datas.toString());

    		con.close();

    		for(Room data:datas){
    			DocumentParser dp = new DocumentParser();
    			dp.formatData(data, query1);
    			dp.tfIdfCalculator(); 
    			Double cs=dp.getCosineSimilarity();
    			data.setCosinesimilarity(cs);

    		}

			//sorting room list with cs

			Collections.sort(datas);

			for (Room r:datas) {
				System.out.println(r.getId()+","+r.getCosinesimilarity());

			}

    		//pass datas and display remaining


			request.setAttribute("roomlist",datas);

			request.getRequestDispatcher("search.jsp").forward(request, response);


		}
    	catch (Exception e)
    	{
    		System.err.println("Got an exception! ");
    		System.err.println(e.getMessage());
    	}
    	
	}
	private String addFacilities(String facilities, String facility) {
		if(facilities!=null) {
			facilities +=",";
		}
		facilities+=facility;
		return facilities;
	
	}
	
	


}
	
    	

	
	
	
    
	


