package com.controller;

import com.dao.RoomDao;
import com.dao.UserDao;
import com.model.LoginBean;
import com.model.Room;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/roominfo.do")
public class RoomInfoServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws ServletException, IOException {
        int roomId = Integer.parseInt(httpServletRequest.getParameter("id"));

        RoomDao roomDao = new RoomDao();
        Room room = roomDao.getRoomById(roomId);

        UserDao userDao = new UserDao();

        LoginBean user = userDao.getUserById(room.getUserId());

        httpServletRequest.setAttribute("roominfo",room);
        httpServletRequest.setAttribute("userinfo", user);

        httpServletRequest.getRequestDispatcher("roominfo.jsp").forward(httpServletRequest, httpServletResponse);





    }
}
