package com.controller;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.dao.LoginDao;
import com.model.LoginBean;

@WebServlet("/login.do")
public class LoginServlet extends HttpServlet {

	public LoginServlet() {

	}

	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String email = request.getParameter("email");
		String pass = request.getParameter("password");

		LoginBean userValidate = LoginDao.authenticateUser(email, pass); // Calling

		if (userValidate == null) {

			// Invalid login
			System.out.println("Invalid Credentials");
			
		}else {

			
			HttpSession session = request.getSession(true);
			session.setAttribute("user_id", userValidate.getId());
			session.setAttribute("user_email", userValidate.getEmail());
			request.getRequestDispatcher("afterlogin.jsp").forward(request, response);
			
		}
	}

}
