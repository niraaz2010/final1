package com.model;

import java.util.Arrays;

public class Room implements Comparable<Room>{
	private int id;
	private String roomType;
	private Double price;
	private String description;
	private String location;
	private String[] facilities;
	private String image;
	private int userId;
	private Double latitude;
	private Double longitude;
	private double cosinesimilarity=0.0;

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}



	public String getRoomType() {
		return roomType;
	}
	public void setRoomType(String roomType) {
		this.roomType = roomType;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String[] getFacilities() {
		return facilities;
	}
	public void setFacilities(String facilities) {
		this.facilities = facilities.split(",");
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}

	public double getCosinesimilarity() {
		return cosinesimilarity;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public void setCosinesimilarity(double cosinesimilarity) {
		this.cosinesimilarity = cosinesimilarity;
	}

	public int compareTo(Room room) {
		if (this.getCosinesimilarity()>room.getCosinesimilarity())
		{
			return -1;
		}
		else
		{
			return 1;
		}
	}
}
