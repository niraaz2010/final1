package com.dao;

import com.connection.Mysql;
import com.model.LoginBean;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class UserDao {

    public LoginBean getUserById(int userId) {
        Connection conn = null;

        try {
            conn = Mysql.createConnection(); // establishing connection

            String sql = "select * from userbean where id=?";
            PreparedStatement pss = conn.prepareStatement(sql);

            pss.setInt(1, userId);

            java.sql.ResultSet rss = pss.executeQuery();
            LoginBean loginBean = null;

            while (rss.next()) {
                loginBean = new LoginBean();
                loginBean.setId(rss.getInt("id"));
                loginBean.setEmail(rss.getString("email"));
                loginBean.setPassword(rss.getString("password"));
                loginBean.setFirstname(rss.getString("firstname"));
                loginBean.setLastname(rss.getString("lastname"));
                loginBean.setAddress(rss.getString("address"));
                loginBean.setPhone(rss.getString("phone"));
            }

            return loginBean;
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }
}
