package com.dao;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import com.connection.Mysql;
import com.model.Room;

import org.apache.commons.lang3.StringUtils;

public class RoomDao {
	public String addRoom(Room room){
		String roomType=room.getRoomType();
		Double price=room.getPrice();
		String description=room.getDescription();
		String location=room.getLocation();
		String facilities= StringUtils.join(room.getFacilities(),",");

		Connection con=null;
		System.out.println();
		try{
			con=Mysql.createConnection();

			String sql="insert into room(roomType,price,description,location,facilities, user_id) values(?,?,?,?,?,?)";

			PreparedStatement  ps = con.prepareStatement(sql);

			ps.setString(1, roomType);
			ps.setDouble(2, price);
			ps.setString(3,description);
			ps.setString(4, location);
			ps.setString(5, facilities);
			ps.setInt(6, room.getUserId());

			int i  = ps.executeUpdate();

			if(i!=0) 
				return "success";

		}

		catch(SQLException e){
			System.out.println(e);
		} 

		
		
		
		return "error";
	}

	public List<Room> getRoomByUser(int userId) {

		try {
			Connection con = Mysql.createConnection();
			String query = "SELECT * from room WHERE user_id=?";
			PreparedStatement statement = con.prepareStatement(query);
			statement.setInt(1, userId);

			ResultSet resultSet;
			resultSet = statement.executeQuery();

			List<Room> roomList = new ArrayList<Room>();

			System.out.println("details");
			while (resultSet.next()) {
				Room room = new Room();
				int id = resultSet.getInt("room_id");
				String rType = resultSet.getString("roomType");
				Double price = resultSet.getDouble("price");
				String description = resultSet.getString("description");
				String location = resultSet.getString("location");
				String rfacilities = resultSet.getString("facilities");

				room.setId(id);
				room.setPrice(price);
				room.setDescription(description);
				room.setRoomType(rType);
				room.setLocation(location);
				room.setFacilities(rfacilities);
				roomList.add(room);

			}
			return roomList;
		}catch (Exception e) {
			System.out.println(e);
			return null;
		}
	}

	public Room getRoomById(int roomId) {

		try {
			Connection con = Mysql.createConnection();
			String query = "SELECT * from room WHERE room_id=?";
			PreparedStatement statement = con.prepareStatement(query);
			statement.setInt(1, roomId);

			ResultSet resultSet;
			resultSet = statement.executeQuery();

			Room room = null;
			while (resultSet.next()) {
				room = new Room();
				int id = resultSet.getInt("room_id");
				String rType = resultSet.getString("roomType");
				Double price = resultSet.getDouble("price");
				String description = resultSet.getString("description");
				String location = resultSet.getString("location");
				String rfacilities = resultSet.getString("facilities");
				int userId = resultSet.getInt( "user_id");

				room.setId(id);
				room.setPrice(price);
				room.setDescription(description);
				room.setRoomType(rType);
				room.setLocation(location);
				room.setFacilities(rfacilities);
				room.setUserId(userId);
			}
			return room;
		}catch (Exception e) {
			System.out.println(e);
			return null;
		}
	}
}
