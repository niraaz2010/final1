package com.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.connection.Mysql;
import com.model.LoginBean;

public class LoginDao {
	public static LoginBean authenticateUser(String email, String pass) {

		Connection conn = null;

		try {
			conn = Mysql.createConnection(); // establishing connection

			String sql = "select * from userbean where email=? and password=?";
			PreparedStatement pss = conn.prepareStatement(sql);

			pss.setString(1, email);
			pss.setString(2, pass);

			java.sql.ResultSet rss = pss.executeQuery();
			LoginBean loginBean = null;
			while (rss.next()) {
				loginBean = new LoginBean();
				loginBean.setId(rss.getInt("id"));
				loginBean.setEmail(rss.getString("email"));
				loginBean.setPassword(rss.getString("password"));
			}

			return loginBean;
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return null;
	}
}
